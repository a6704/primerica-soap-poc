1. Install SoapUI in your machine
2. Add jxl.jar to SoapUI/lib folder
3. Add poi-ooxml.jar to SoapUI/bin/ext
4. Hard coded the below paths for POC purpose, which can be parametrized in future. Copy/Place the Input and IBA result files

C:\API_Automation\SoapUI\IBA\Data\Input.xls (update the path and folder as per the folder direcory that you have created in your machine, in data driver groovy script)
C:\API_Automation\SoapUI\IBA\Result\IBA.xls (update the path and folder as per the folder direcory that you have created in your machine, in Result groovy script)

5. Import the REST-Project-1-soapui-project.xml in to Soap UI, if you would like to execute from Soap UI
6. Created the base pom.xml file for Integration with Maven which can be initiated from Jenkin, as part of CICD process. Looking for the primerica URL's to complete it.